#!/bin/bash
MODEL_PATHS=./saved_models/*
for model_path in $MODEL_PATHS; do
    sbatch test.sh $model_path
done

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --partition=batch

source ~/virtualenvs/xcs/bin/activate
python test.py $1 
mv "slurm-${SLURM_JOB_ID}.out" "./test_slurm_logs"

#!/usr/bin/python
import glob
import parse as ps
import operator

PERFORMANCE_CUTOFF=195.0

def main():
    print("Models above performance cutoff of {}:".format(PERFORMANCE_CUTOFF))
    performances = {}
    for test_log in glob.glob("./test_slurm_logs/*"):
        with open(test_log, "r") as fp:
            model_path = None
            avg_steps = None
            for line in fp:
                if "Testing on" in line:
                    model_path = ps.parse("Testing on {}", line)[0]
                if "Average number" in line:
                    avg_steps = float(ps.parse("Average number of steps balanced over {} episodes: {}", line)[1])
                    if avg_steps >= PERFORMANCE_CUTOFF:
                        print(model_path, avg_steps)
            performances[model_path] = avg_steps

    print("All models in order of performance:")
    # create list of tuples representing sorted dictionary
    sorted_performances = sorted(performances.items(), key=operator.itemgetter(1), reverse=True)
    for elem in sorted_performances:
        print(elem[0], elem[1])

if __name__ == "__main__":
    main()

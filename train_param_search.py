#!/usr/bin/python
import subprocess
import time
import numpy as np

def gen_param_vals():
    param_vals = {}
    param_vals["training_cycles"] = 100000
    param_vals["max_pop_size"] = np.random.choice([1000, 2000, 3000, 4000])
    param_vals["wildcard_prob"] = np.random.uniform(0.1, 0.5)
    param_vals["error_threshold"] = np.random.uniform(0, 0.01)
    param_vals["learning_rate"] = np.random.uniform(0.1, 0.2)
    param_vals["bits_per_feature"] = np.random.choice([3, 4, 5, 6])
    param_vals["discount_factor"] = np.random.uniform(0.71, 1) 
    param_vals["action_set_subsumption"] = np.random.choice([0, 1])
    param_vals["egreedy_strategy"] = np.random.choice([1, 2])
    return param_vals

def main():
    combos_to_run = 100
    job_limit = 20
    while combos_to_run > 0:
        jobs = subprocess.run(["squeue","-u","uqjbish3"], stdout=subprocess.PIPE).stdout.decode("utf-8")
        num_jobs = len(jobs.split("\n"))-1
        if num_jobs < job_limit:
           params = gen_param_vals()
           for seed in range(10):
               subprocess.Popen("sbatch train.sh {}" \
                   " {} {} {} {} {} {} {} {} {}".format(params["training_cycles"],
                       params["max_pop_size"],
                       params["wildcard_prob"],
                       params["error_threshold"],
                       params["learning_rate"],
                       params["bits_per_feature"],
                       params["discount_factor"],
                       params["action_set_subsumption"],
                       params["egreedy_strategy"],
                       seed),
                   shell=True)
           combos_to_run -= 1
        time.sleep(1)

if __name__ == "__main__":
    main()

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --partition=batch

source ~/virtualenvs/xcs/bin/activate

if [ $8 -eq 0 ]; then
    # action set subsumption on
    if [ $9 -eq 1 ]; then
        # balanced egreedy
        python train.py ${SLURM_JOB_ID} balanced_egreedy --training-cycles=$1 --max-pop-size=$2 \
            --wildcard-prob=$3 --error-threshold=$4 --learning-rate=$5 \
            --bits-per-feature=$6 --discount-factor=$7 --seed=${10} \
            --ga-subsumption --action-set-subsumption
    elif [ $9 -eq 2 ]; then
        # linear decay egreedy
        python train.py ${SLURM_JOB_ID} linear_decay_egreedy --training-cycles=$1 --max-pop-size=$2 \
            --wildcard-prob=$3 --error-threshold=$4 --learning-rate=$5 \
            --bits-per-feature=$6 --discount-factor=$7 --seed=${10} \
            --ga-subsumption --action-set-subsumption
    fi
elif [ $8 -eq 1 ]; then
    # action set subsumption off
    if [ $9 -eq 1 ]; then
        # balanced egreedy
        python train.py ${SLURM_JOB_ID} balanced_egreedy --training-cycles=$1 --max-pop-size=$2 \
            --wildcard-prob=$3 --error-threshold=$4 --learning-rate=$5 \
            --bits-per-feature=$6 --discount-factor=$7 --seed=${10} \
            --ga-subsumption
    elif [ $9 -eq 2 ]; then
        # linear decay egreedy
        python train.py ${SLURM_JOB_ID} linear_decay_egreedy --training-cycles=$1 --max-pop-size=$2 \
            --wildcard-prob=$3 --error-threshold=$4 --learning-rate=$5 \
            --bits-per-feature=$6 --discount-factor=$7 --seed=${10} \
            --ga-subsumption
    fi
fi

mv "slurm-${SLURM_JOB_ID}.out" "./train_slurm_logs"

#!/usr/bin/python
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import parse as ps

def main():
    avg_performances = []
    with open("parsed.txt", "r") as fp:
        for line in fp:
            if "pkl" in line:
                res = ps.parse("{} {}", line)
                avg_performance = float(res[1])
                avg_performances.append(avg_performance)
    plt.hist(avg_performances, bins=20)
    plt.title("Cartpole average performance histogram")
    plt.savefig('cartpole_avg_perf_hist.png')

if __name__ == "__main__":
    main()

Q-value estimates for left/right action seem to be ok... over 19 when
aggregating info from all classifiers. But dunno if the distinction between
them is enough to give decent control. Going to try upping the number of
training cycles and increasing number of bits per feature to make
discretisation less coarse.

31/1/19

Figured out the reason why max Q val estimate is 19.999...
it's because discount factor is 0.95, reward on each step is 1, so it's a
geometric series from i=0 to 200 of 0.95^i -> converges to 19.999...
So 200 timesteps is max it can go for. That makes so much more sense than 20
haha

Apparently env is solved if average reward is 195 or more over 100 trials, so I
still have a bit to go. But getting to 200 sometimes is promising. High
variance in trials atm though. 

118671 is pretty good
118674 is also pretty good

now trying to modify xcs code to track internals of algorithm
Need to track:

- Covering: rate of covering should drop sharply as training progresses
- Classifier length of existence + amount of experience.
- Number of macroclassifiers, number of microclassifiers
- Rate of subsumption
- Rate of deletion

PopulationTracker class??

1/2/19

Ok, so I put in tracking for:
- macros
- micros
- avg generality
- covering
- deletion
- ga subsumptions
- action set subsumptions

am now going to run a big batch of experiments... wish me luck

5/2/19

Writing some code to calc average num steps balanced for over 100 testing
episodes. Then need to put on all pickled models and parse their slurm outputs
to see which ones are good.

First though, need to somehow  figure out how to get bits per feature info from
the model, as manually specifying it is stupid.
Easy... just get first classifier in pop and look at len of its condition
string

6/2/19

Ok so the testing is done. Params for best and worst performers:

Best - 119164
Namespace(accuracy_coefficient=0.1, accuracy_power=5, action_set_subsumption=False, bits_per_feature=4, crossover_prob=0.8, deletion_threshold=20, discount_factor=0.95, error_threshold=0.01, explore_prob=0.5, ga_subsumption=True, ga_threshold=25, learning_rate=0.1, max_pop_size=3000, mutation_prob=0.4, seed=2, subsumption_threshold=20, tag='119164', training_cycles=10000, wildcard_prob=0.2)
Macros 3000, Micros 3000 at last checkpoint
This one is overfitting like crazy but since it's RL it doesn't get penalised for that.
No subsumption at all

Worst - 120042 (tied with heaps of others but at bottom of list since highest number)
Namespace(accuracy_coefficient=0.1, accuracy_power=5, action_set_subsumption=True, bits_per_feature=3, crossover_prob=0.8, deletion_threshold=20, discount_factor=0.95, error_threshold=0.01, explore_prob=0.5, ga_subsumption=True, ga_threshold=25, learning_rate=0.1, max_pop_size=4000, mutation_prob=0.4, seed=0, subsumption_threshold=20, tag='120042', training_cycles=100000, wildcard_prob=0.4)
Macros 805, Micros 4000 at last checkpoint
Macros and avg generality steadily decreased from about checkpoint 330/1000, ~1/3 of the way through training, correlating with steady stream of GA subsumptions
So basically this one is underfitting? Even though its avg generality is going down, a few very general rules are dominating I think... now sure how though
It's bad because it has stupid rules like all wildcards mapping to 0 and all to 1. So I think what's happening is that somehow these terrible rules are gaining fitness and subsuming newly born rules almost immediately.
Watching visual testing, it also goes left 100% of the time and instantly fails


Also... i thought about the seed for testing. If some of the agents are trained with seed 0 for gym, they probs shouldn't be tested with seed 0, otherwise it's the exact same data. Should probs use diff seed for testing.
I want to see now what effect rule compaction will have on the best one. Maybe use a fitness cutoff + experience cutoff?
So I'm trying to figure out how to do the reduction/compaction...
Seems like there is an original alg for XCS by wilson that got superseded by one made by Dixon - "CRA2". So i'm looking at CRA2 first to see what it is.

But wait... before that it'd probs be interesting to see how the different seeded versions of the best model did. If they are similar, i guess it would indicate that those params are decent. If they are not similar, params are probs bad.

119164 was seed 2, so need to look at
119162 - seed 0 - avg perf = 21.88
119163 - seed 1 - avg perf = 15.32
119164 - seed 2 - avg perf = 161.35
119165 - seed 3 - avg perf = 13.62
119166 - seed 4 - avg perf = 34.0
Wow, that's a huge spread.


Now let's look at the different seeded versions of the worst classifier:
120042 was seed 0, so need to look at
120042 - seed 0 - avg perf = 9.31
120043 - seed 1 - avg perf = 9.4
120044 - seed 2 - avg perf = 9.31
120045 - seed 3 - avg perf = 9.31
120046 - seed 4 - avg perf = 9.4 
So for all the seeds this ones sucks...

Now how about the top 5 performers compared in terms of parameters?
1st - 119164
Namespace(accuracy_coefficient=0.1, accuracy_power=5, action_set_subsumption=False, bits_per_feature=4, crossover_prob=0.8, deletion_threshold=20, discount_factor=0.95, error_threshold=0.01, explore_prob=0.5, ga_subsumption=True, ga_threshold=25, learning_rate=0.1, max_pop_size=3000, mutation_prob=0.4, seed=2, subsumption_threshold=20, tag='119164', training_cycles=10000, wildcard_prob=0.2)
2nd - 118995
Namespace(accuracy_coefficient=0.1, accuracy_power=5, action_set_subsumption=False, bits_per_feature=4, crossover_prob=0.8, deletion_threshold=20, discount_factor=0.95, error_threshold=0.01, explore_prob=0.5, ga_subsumption=True, ga_threshold=25, learning_rate=0.1, max_pop_size=2000, mutation_prob=0.4, seed=3, subsumption_threshold=20, tag='118995', training_cycles=10000, wildcard_prob=0.4)
3rd - 120071
Namespace(accuracy_coefficient=0.1, accuracy_power=5, action_set_subsumption=True, bits_per_feature=4, crossover_prob=0.8, deletion_threshold=20, discount_factor=0.95, error_threshold=0.01, explore_prob=0.5, ga_subsumption=True, ga_threshold=25, learning_rate=0.1, max_pop_size=4000, mutation_prob=0.4, seed=4, subsumption_threshold=20, tag='120071', training_cycles=10000, wildcard_prob=0.3)
4th - 119112 
Namespace(accuracy_coefficient=0.1, accuracy_power=5, action_set_subsumption=False, bits_per_feature=6, crossover_prob=0.8, deletion_threshold=20, discount_factor=0.95, error_threshold=0.01, explore_prob=0.5, ga_subsumption=True, ga      _threshold=25, learning_rate=0.1, max_pop_size=2000, mutation_prob=0.4, seed=0, subsumption_threshold=20, tag='119112', training_cycles=1000000, wildcard_prob=0.4)
5th - 119578
Namespace(accuracy_coefficient=0.1, accuracy_power=5, action_set_subsumption=True, bits_per_feature=5, crossover_prob=0.8, deletion_threshold=20, discount_factor=0.95, error_threshold=0.01, explore_prob=0.5, ga_subsumption=True, ga_t     hreshold=25, learning_rate=0.1, max_pop_size=1000, mutation_prob=0.4, seed=1, subsumption_threshold=20, tag='119578', training_cycles=10000, wildcard_prob=0.4)
    as_sub | bits_per_feature | max_pop_size | training_cycles | wildcard_prob
    --------------------------------------------------------------------------
1st |  F   |       4          |     3k       |       10k       |     0.2
2nd |  F   |       4          |     2k       |       10k       |     0.4
3rd |  T   |       4          |     4k       |       10k       |     0.3
4th |  F   |       6          |     2k       |       1M        |     0.4
5th |  T   |       5          |     1k       |       10k       |     0.4

So it seems like the algorithm is very sensitive to the random seed.

What about decaying epsilon over time for action selection, like in standard RL? Remembering that GA is only invoked in explore steps.

7/2/19

Let's say 50% of the episodes should be exploring, and 50% should be exploiting with prob 0.1, so halfway point in the number of episodes is what I'd need as the cutoff of the gradient?
This *should* better deal with exploit/explore, since we will be exploring more before exploiting. And then if it doesn't explore enough, just ramp up the number of episodes.

10000   roughly 5000 explore, 5000 constant epsilon -> want x^5000 = 0.1 -> x ~= 0.99954
100000  roughly 50000 explore, 50000 constant epsilon 
1000000 10x above

Ok, so I implemented linear decay e greedy.
Deleted all the original experimental data, and now going to start from scratch with bigger hyperparam search and more seeds

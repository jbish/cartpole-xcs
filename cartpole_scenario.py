from xcs.scenarios import Scenario
from xcs.bitstrings import BitString
import numpy as np
import gym

class CartpoleProblem(Scenario):
    
    def __init__(self, cycles, bits_per_feature, seed, testing=False,
            render_screen=False):
        self.env = gym.make("CartPole-v0")
        self.env.seed(seed)
        self.possible_actions = (0, 1)
        self.testing = testing
        if self.testing:
            self.testing_ep_steps = 0
            self.steps_balanced = []
        self.render_screen = render_screen
        self.init_cycles = cycles
        self.remaining_cycles = cycles
        self.curr_obs = self.env.reset()
        self.bits_per_feature = bits_per_feature
        self.num_buckets = 2**self.bits_per_feature

    @property
    def is_dynamic(self):
        return True

    def get_possible_actions(self):
        return self.possible_actions

    def reset(self):
        self.remaining_cycles = self.init_cycles
        self.curr_obs = self.env.reset()

    def more(self):
        return self.remaining_cycles > 0

    def sense(self):
        # discretise obs and use gray encoding to get bits
        discretised_obs = self.discretise_obs(self.curr_obs)
        encoded_obs = self.encode_obs(discretised_obs)
        return BitString(encoded_obs)

    def execute(self, action):
        obs, reward, done, _ = self.env.step(action)
        if self.testing:
            self.testing_ep_steps += 1
        if self.render_screen:
            self.env.render()
        self.curr_obs = obs
        # If training, treat each environmental interaction as a cycle.
        # If testing, treat an episode as a cycle to allow statistics to be
        # calculated properly.
        if not self.testing:
            self.remaining_cycles -= 1
        if done:
            self.curr_obs = self.env.reset()
            if self.testing:
                print("Balanced pole for {0} steps".format(self.testing_ep_steps))
                self.steps_balanced.append(self.testing_ep_steps)
                self.remaining_cycles -= 1
                self.testing_ep_steps = 0
                if not self.more():
                    # print out average steps over all runs
                    print("Average number of steps balanced over {0} episodes:"
                        " {1}".format(self.init_cycles,
                            sum(self.steps_balanced)/len(self.steps_balanced)))
        return reward

    def discretise_obs(self, obs):
        # Discretise each dimension so that the out of bounds/outside normal range values are a single bucket, and apply the desired number of buckets to only the valid range
        # Cart x position: [-4.8, -2.4) out of bounds, [-2.4, 2.4] in bounds, (2.4, 4.8] out of bounds
        # Cart velocity: [-max_float32_val, -2.5) outside normal range, [-2.5, 2.5] in normal range, (2.5, max_float32_val) outside normal range
        # Pole angle: [-2*pi/15, -pi/15) out of bounds, [-pi/15, pi/15] in bounds, (pi/15, 2*pi/15] out of bounds
        # Pole angular velocity: [-max_float32_val, -3.5) outside normal range, [-3.5, 3.5] in normal range, (3.5, max_float32_val] outside normal range

        cart_x_pos_lb = -2.4  # valid region lower bound
        cart_x_pos_ub = 2.4  # valid region upper bound
        cart_x_pos_bs = (cart_x_pos_ub - cart_x_pos_lb) / self.num_buckets
        cart_x_vel_lb = -2.5
        cart_x_vel_ub = 2.5
        cart_x_vel_bs = (cart_x_vel_ub - cart_x_vel_lb) / self.num_buckets
        pole_ang_lb = -np.pi/15
        pole_ang_ub = np.pi/15
        pole_ang_bs = (pole_ang_ub - pole_ang_lb) / self.num_buckets
        pole_ang_vel_lb = -3.5
        pole_ang_vel_ub = 3.5
        pole_ang_vel_bs = (pole_ang_vel_ub - pole_ang_vel_lb) / self.num_buckets

        # Calc new obs values
        # cart x pos
        if obs[0] < cart_x_pos_lb:
            cart_x_pos = 0
        elif obs[0] > cart_x_pos_ub:
            cart_x_pos = self.num_buckets-1 # -1 because indexing from 0 and result has to be binary encoded
        else:
            cart_x_pos = int((obs[0] - cart_x_pos_lb) // cart_x_pos_bs)
        # cart x vel
        if obs[1] < cart_x_vel_lb:
            cart_x_vel = 0
        elif obs[1] > cart_x_vel_ub:
            cart_x_vel = self.num_buckets-1
        else:
            cart_x_vel = int((obs[1] - cart_x_vel_lb) // cart_x_vel_bs)
        # pole ang
        if obs[2] < pole_ang_lb:
            pole_ang = 0
        elif obs[2] > pole_ang_ub:
            pole_ang = self.num_buckets-1
        else:
            pole_ang = int((obs[2] - pole_ang_lb) // pole_ang_bs)
        # pole ang vel
        if obs[3] < pole_ang_vel_lb:
            pole_ang_vel = 0
        elif obs[3] > cart_x_vel_ub:
            pole_ang_vel = self.num_buckets-1
        else:
            pole_ang_vel = int((obs[3] - pole_ang_vel_lb) // pole_ang_vel_bs)
        return [cart_x_pos, cart_x_vel, pole_ang, pole_ang_vel]

    def encode_obs(self, obs):
        encoded_obs = []
        for elem in obs:
            gray_coded = elem^(elem>>1)
            # add extra zeros at front to fill in all bits
            formatted = list('{{:{0}>{1}}}'.format(0, self.bits_per_feature).format(bin(gray_coded)[2:]))
            formatted = [int(i) for i in formatted] # convert strings to ints
            encoded_obs.extend(formatted)
        return encoded_obs

#!/usr/bin/python
from cartpole_scenario import CartpoleProblem
import xcs
import logging
from xcs.scenarios import ScenarioObserver
import pickle
import argparse

NUM_CARTPOLE_FEATURES=4

def main():
    args = parse_args()
    # Load model from disk
    print("Testing on {}".format(args.modelpath))
    with open(args.modelpath, "rb") as fp:
        model = pickle.load(fp)

    # Rule compaction
    #model.compact(strategy={"accuracy_numerosity_product_threshold": 5})
    #model.compact(strategy={"fitness_numerosity_product_threshold": 0.25})
    print(model)
    # Test
    # Use first classifier from population to figure out how many bits per
    # feature
    bits_per_feature = None
    for classifier in model:
        condition = str(classifier.condition)
        bits_per_feature = int(len(condition)/NUM_CARTPOLE_FEATURES)
        break
    problem = CartpoleProblem(args.testing_cycles, bits_per_feature,
            args.seed, testing=True, render_screen=args.render_screen)
    scenario = ScenarioObserver(problem)
    model.test(scenario)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("modelpath")
    parser.add_argument("--testing-cycles", type=int, default=100)
    parser.add_argument("--seed", type=int, default=0) # 0 will not collide with any training seeds
    parser.add_argument("--render-screen", action="store_true")
    return parser.parse_args()

if __name__ == "__main__":
    main()

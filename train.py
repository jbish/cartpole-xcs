#!/usr/bin/python
from cartpole_scenario import CartpoleProblem
import xcs
import logging
from xcs.scenarios import ScenarioObserver
from xcs.framework import LinearDecayEpsilonGreedySelectionStrategy
import argparse
import random
import pickle

def main():
    args = parse_args()
    print("Experiment params:")
    print(args)

    random.seed(args.seed)
    logging.root.setLevel(logging.INFO)
    algorithm = xcs.XCSAlgorithm()

    # Modified parameter settings - suggested in Intro to learning classifier systems
    # Often changeable
    algorithm.max_population_size = args.max_pop_size
    algorithm.wildcard_probability = args.wildcard_prob
    # Moderately changeable
    algorithm.error_threshold = args.error_threshold
    algorithm.accuracy_power = args.accuracy_power
    algorithm.learning_rate = args.learning_rate
    # Rarely changeable
    algorithm.ga_threshold = args.ga_threshold
    algorithm.mutation_probability = args.mutation_prob
    algorithm.crossover_probability = args.crossover_prob
    algorithm.deletion_threshold = args.deletion_threshold
    algorithm.subsumption_threshold = args.subsumption_threshold
    # Never changeable
    algorithm.accuracy_coefficient = args.accuracy_coefficient
    # Other
    algorithm.do_ga_subsumption = args.ga_subsumption
    algorithm.do_action_set_subsumption = args.action_set_subsumption

    # env specific settings
    algorithm.discount_factor = args.discount_factor
    if args.exploretype == "balanced_egreedy":
        algorithm.exploration_probability = 0.5
    elif args.exploretype == "linear_decay_egreedy":
        # calc gradient 
        # rearrangement of y = mx+c to solve for grad when cutoff is reached halfway through training cycles
        epsilon_grad = (args.epsilon_cutoff - args.epsilon_init)/(args.training_cycles/2)
        strategy = LinearDecayEpsilonGreedySelectionStrategy(grad=epsilon_grad, cutoff=args.epsilon_cutoff, epsilon=args.epsilon_init)
        algorithm.exploration_strategy = strategy
    else:
        raise ValueError("Invalid exploration type")

    problem = CartpoleProblem(args.training_cycles, args.bits_per_feature,
            args.seed)
    scenario = ScenarioObserver(problem)
    algorithm.minimum_actions = len(problem.get_possible_actions())
    model = algorithm.new_model(scenario)
    # Train
    model.run(scenario, learn=True)
    print(model)
    # Save model to disk
    with open("./saved_models/{0}.pkl".format(args.tag), "wb") as fp:
        pickle.dump(model, fp)

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("tag")
    parser.add_argument("exploretype")
    # Often changeable
    parser.add_argument("--training-cycles", type=int, default=10000)       # x10, more data should always be better at the cost of more compute time
    parser.add_argument("--max-pop-size", type=int, default=1000)           # +- 1000, increasing gives model more capacity but decreases pressure to form compact solution
    parser.add_argument("--wildcard-prob", type=float, default=0.3)         # +- 0.1, should be set as low as possible while still allowing covering to only happen at start of learning,
                                                                            # as high values of P_# will make covering be activated less frequently as more example space is covered with
                                                                            # each new classifier generated. So, increase if covering is invoked too much, and decrease if output model
                                                                            # is too general.
    # Moderately changeable
    parser.add_argument("--error-threshold", type=float, default=0.01)      # +- 0.01, no noise in cartpole so could reduce to 0. Needs to be set in conjunction with learning rate.
    parser.add_argument("--accuracy-power", type=int, default=5)            # +- 1
    parser.add_argument("--learning-rate", type=float, default=0.1)         # +- 0.02, shouldn't set higher than 0.2, should be higher for more straightforward problems with low noise.
    # Rarely changeable
    parser.add_argument("--ga-threshold", type=int, default=25)             # +- 5
    parser.add_argument("--mutation-prob", type=float, default=0.4)         # +- 0.1
    parser.add_argument("--crossover-prob", type=float, default=0.8)        # +- 0.1
    parser.add_argument("--deletion-threshold", type=int, default=20)       # +- 5
    parser.add_argument("--subsumption-threshold", type=int, default=20)    # +- 5
    # Never changeable
    parser.add_argument("--accuracy-coefficient", type=float, default=0.1)
    # Others
    parser.add_argument("--ga-subsumption", action="store_true")
    parser.add_argument("--action-set-subsumption", action="store_true")
    parser.add_argument("--discount-factor", type=float, default=0.95)      # 0.71 reported good results in literature, +- 0.05?
    parser.add_argument("--bits-per-feature", type=int, default=3)
    parser.add_argument("--seed", type=int, default=0)
    parser.add_argument("--epsilon-init", type=float, default=1.0)
    parser.add_argument("--epsilon-cutoff", type=float, default=0.1)
    return parser.parse_args()

if __name__ == "__main__":
    main()
